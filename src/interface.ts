import TodoService from "./controller/todoController";

export interface TodoElementInterface {
  id?: number;
  text: string;
  done: boolean;
  editTodo(event: any): void;
  setDone(event: any): void;
 
  
}

export interface TodoInterface{
  id?: number;
  text: string;
  done: boolean;
}

export interface TodoListState {
  todos: Array<TodoInterface>;
  text: string;
  editId?: number;
}
