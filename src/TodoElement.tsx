import { TodoElementInterface } from "./interface";
import React from "react";

export function TodoImpl(props: TodoElementInterface): JSX.Element {
  const ren = (
    <tr key={props.id} data-id={props.id} onClick={props.editTodo}>
      <th className="todoId">{props.id}</th>
      <th className="todo">{props.text}</th>
      <th className="todoButton">
        {props.done ? (
          ""
        ) : (
          <button data-id={props.id} onClick={props.setDone}>
            Splněno
          </button>
        )}
      </th>
    </tr>
  );

  return ren;
}

export default TodoImpl;
