import React from 'react';
import ReactDOM from 'react-dom';
import TodoListElement from './TodoListElement';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  
  <React.StrictMode>
    <h1>Todo List</h1>
    <TodoListElement></TodoListElement>    
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
