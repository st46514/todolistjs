import http from "./common";
import { TodoInterface } from "../interface";
import Todo from "../entity/Todo";

class TodoController {
  async getAll(): Promise<Array<TodoInterface>> {
    const todosGet = await http
      .get<Array<TodoInterface>>("/todos")
      .then((data) => data.data);
    const todos = new Array<TodoInterface>();
    todosGet.map(
      (todo, index) =>
        (todos[index] = new Todo({
          id: todo.id,
          text: todo.text,
          done: todo.done,
        }))
    );
    return todos;
  }
  async save(todo: Todo): Promise<TodoInterface> {
    return await http.post<TodoInterface>('/todo',todo).then((data) => data.data);
  }
  async edit(todo: Todo): Promise<TodoInterface>{
    return await http.put<TodoInterface>('/todo',todo,{params: {'id': todo.id}}).then((data) => data.data);
  }
}
export default TodoController;
