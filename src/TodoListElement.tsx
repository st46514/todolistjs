import { TodoInterface, TodoListState } from "./interface";
import TodoController from "./controller/todoController";

import React from "react";

import TodoElement from "./TodoElement";
import "./TodoListElement.css";
import Todo from "./entity/Todo";

class TodoListElement extends React.Component<{}, TodoListState> {
  todoController: TodoController;
  constructor(props: {}) {
    super(props);
    this.todoController = new TodoController();
    this.state = {
      todos: Array<TodoInterface>(),
      text: "",
      editId: undefined,
    };
    this.addOrUpdateTodo = this.addOrUpdateTodo.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.editTodo = this.editTodo.bind(this);
    this.setDone = this.setDone.bind(this);
  }
  componentDidMount(): void {
    this.todoController.getAll().then((data): void =>
      this.setState({
        todos: data,
      })
    );
  }
  setDone(event: React.MouseEvent<HTMLButtonElement>): void {
    event.preventDefault();
    const stateTodos = this.state.todos.slice();
    const id = Number(event.currentTarget.dataset.id);
    const todoIndex = this.state.todos.findIndex((todo) => todo.id === id);
    const todo = stateTodos[todoIndex];
    todo.done = true;
    this.todoController.edit(todo);

    this.setState({
      todos: stateTodos,
      text: "",
    });
  }

  editTodo(event: React.MouseEvent<HTMLTableRowElement, MouseEvent>): void {
    event.preventDefault();
    const id = Number(event.currentTarget.dataset.id);
    const todo = this.state.todos.find((todo) => todo.id === id);
    if (todo !== undefined && !todo.done) {
      this.setState({
        text: todo.text,
        editId: id,
      });
    }
  }
  handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const newText = event.currentTarget.value;
    this.setState({ text: newText });
  }

  addOrUpdateTodo(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    if (this.state.text.length === 0) {
      return;
    }
    let stateTodos = this.state.todos.slice();
    if (this.state.editId !== undefined) {
      const todoIndex = this.state.todos.findIndex(
        (todo) => todo.id === this.state.editId
      );
      const todo = stateTodos[todoIndex];
      todo.text = this.state.text;
      this.todoController.edit(todo);
    } else {
      const todo = new Todo({
        text: this.state.text,
        done: false,
      });
      this.todoController
        .save(todo)
        .then((todo): void => {
          stateTodos = this.state.todos.concat(todo);
          this.setState({ todos: stateTodos });
        })
        .catch((err) => console.log(err));
    }
    this.setState({
      todos: stateTodos,
      text: "",
      editId: undefined,
    });
  }
  render(): JSX.Element {
    const todos = this.state.todos.map((todo) => (
      <TodoElement
        key={todo.id}
        id={todo.id}
        text={todo.text}
        done={todo.done}
        editTodo={(event: React.MouseEvent<HTMLTableRowElement, MouseEvent>): void => this.editTodo(event)}
        setDone = {(event: React.MouseEvent<HTMLButtonElement>): void=>this.setDone(event)}
      ></TodoElement>
    ));
    return (
      <div className="todoContainer">
        <form className="todoForm" onSubmit={this.addOrUpdateTodo}>
          <input
            type="text"
            value={this.state.text}
            onChange={this.handleChange}
          ></input>
          <input type="submit" value="Odeslat" />
        </form>

        <div className="todoList">
          <table>
            <tbody>{todos}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default TodoListElement;
