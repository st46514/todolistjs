import { TodoInterface } from "../interface";

class Todo implements TodoInterface {
    id?: number;
    text: string;
    done: boolean;
    constructor(props: TodoInterface){
        this.id = props.id;
        this.text = props.text;
        this.done = props.done;
        
    }

}

export default Todo